import sqlite3
import sys
import uuid
import time
import json

import bottle


#@bottle.get("/<file>")
#def home(file):
#    bottle.static_file(file, ".")

@bottle.post("/register")
def register():
    return str(uuid.uuid4())

@bottle.post("/<account_uuid>/<type:int>")
def update(account_uuid: str, type: int):
    with sqlite3.connect("database.sqlite") as conn:
        cur = conn.cursor()
        cur.execute("INSERT INTO history(id, type, encrypted_data, timestamp) VALUES(?,?,?,?)",
                    (account_uuid,
                     type,
                     json.dumps(bottle.request.json),
                     time.time()))
        conn.commit()

@bottle.get("/<account_uuid>/<type:int>")
def get_history_data(account_uuid: str, type: int):
    with sqlite3.connect("database.sqlite") as conn:
        cur = conn.cursor()
        cur.execute("SELECT encrypted_data FROM history WHERE id = ? AND type = ?", (account_uuid, type,))
        bottle.response.content_type = 'application/json'
        items = []
        for row in cur.fetchall():
            try:
                items.append(json.loads(row[0]))
            except json.decoder.JSONDecodeError as e:
                print("Failed to decode json object", file=sys.stderr)
        return json.dumps(items)


with sqlite3.connect("database.sqlite") as conn:
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS history(
            id UUID,
            type SMALLINT,
            encrypted_data TEXT,
            timestamp TIMESTAMP
        );
    """)
    cur.execute("CREATE INDEX IF NOT EXISTS index_id ON history(id);")
    cur.execute("CREATE INDEX IF NOT EXISTS index_type ON history(type);")
    conn.commit()

bottle.run(host="0.0.0.0")
