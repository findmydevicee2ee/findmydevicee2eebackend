

# Server Backend

The server backend has been implemented using Python version 3 with the Flask-like microframework Bottlepy.

It uses an SQL database to store and retrieve sent location data and commands such as remote wipe commands. All of this data is end-to-end encrypted and undecipherable for the server. There is however still some metadata that is stored alongside this encrypted data.

## Metadata

### The associated UUIDs of the encrypted data

This is important because this will allow the server to return the relevant encrypted data to the respective user when necessary.

### A timestamp of the encrypted data upload

This timestamp is primarily stored so the server can determine how old the data is, so it can dispose of it, once a certain amount of time has passed.

### The "type" of an encrypted data upload

This field makes it possible to quickly differentiate whether the data was meant to be read by a recovery web UI, when the device has been lost or if the data is supposed to be received by the device that may have gone missing.

The latter may for example be a remote wipe command to the device. To ensure that only the owner of the device is actually able to send such a wipe command, that command will also have to have to be encrypted and have a valid MAC as well.

## Database

The database that is currently used is a SQLite3 database using the DB-API 2.0. This database has been chosen because it's the simplest SQL-based database to use for testing-purposed since the database is just a single file and no database server has to be setup to run alongside the server.

It is however recommended that servers that run in a production environment use a PostgreSQL server instead, since Postgres does proper data type validation and offers better performance at scale.

Since DB-API 2.0 has been used, the changes that would be necessary to switch to PostgreSQL are very minimal.

## Concerns

### Device registration

Currently there are no limitations in place, that limit device registrations.

The lack of a device registration limitation shouldn't really be much of a problem, the way things are currently implemented, since the server just simply returns a random UUID each time.

While this theoretically could lead to a UUID collision, the chances of that happening are vanishingly low. On the off chance that this did happen on super rare occasions however, it wouldn't be that big of an issue either. Since that would just mean that different devices send data to the server with the same UUID. This causes the server to associate the encrypted data of different devices with the same UUID. This may cause a user that has lost their device and tries to locate it on the web UI to retrieve data that was not meant for them and devices to potentially receive remote wipe commands that weren't actually meant for them. But since all of this data is encrypted and authenticated with a MAC, the device or the web UI would just recognize it as invalid, malformed data and discard it without any visible or potentially even dangerous effect.

### Spam updates

The backend doesn't currently offer any built-in counter-measures against malicious users that spam update requests to the server, which could potentially waste a lot of disk space and processing power. It may be a good idea to setup some kind of DDoS protection.

This is a difficult issue, since identifying a user could be a very effective way to combat spam updates, it would however also limit their privacy and anonymity.

### Automatic cleanup

Currently there are no cleanup routines in place that would automatically delete old user data. Adding this functionality should be fairly easy however because of the timestamp field that's stored alongside every user data upload.

Choosing a sensible cleanup interval that's not too short would be an important factor to consider to ensure that users still manage to track their device in time and so that wipe commands have a realistic chance to arrive on lost devices.

This data could also be interesting for users to keep long-term to track and potentially share their own location history with other people in future versions of the app or web UI, which is another reason why this has not been implemented.
